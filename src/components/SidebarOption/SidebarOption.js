import { useContext } from "react";
import { useDispatch } from "react-redux";
import { addDoc, collection, serverTimestamp } from "firebase/firestore";
import styled from "styled-components";
import { ThemeContext } from "../../contexts/ThemeContext";
import { enterRoom } from "../../redux/reducers/appSlice";
import { db } from "../../firebase";

function SidebarOption({ Icon, label, triggerAction, id }) {
  const dispatch = useDispatch();
  const { theme } = useContext(ThemeContext);

  const addChannel = async () => {
    const channelToAdd = prompt("Enter channel name");

    if (channelToAdd) {
      await addDoc(collection(db, "channels"), {
        name: channelToAdd,
        timestamp: serverTimestamp(),
      });
    }
  };

  const selectChannel = () => {
    if (id) {
      dispatch(
        enterRoom({
          roomId: id,
        })
      );
    }
  };

  return (
    <SidebarOptionContainer
      theme={theme}
      onClick={triggerAction ? addChannel : selectChannel}
    >
      {Icon ? (
        <>
          <Icon fontSize="small" style={{ margin: 12 }} />
          <h3>{label}</h3>
        </>
      ) : (
        <SidebarOptionChannel>
          <span style={{ margin: 12 }}>#</span> {label}
        </SidebarOptionChannel>
      )}
    </SidebarOptionContainer>
  );
}

export default SidebarOption;

const SidebarOptionContainer = styled.div`
  display: flex;
  align-items: center;
  margin: 10px;
  border-radius: 7px;
  cursor: pointer;
  position: relative;

  :hover {
    background-color: ${(props) =>
      props.theme === "dark"
        ? "var(--secondary-color-dark)"
        : "var(--secondary-color-light)"};
  }

  > h3 {
    font-size: 15px;
    font-weight: 500;
  }
`;

const SidebarOptionChannel = styled.div`
  padding: 10px 0;
  font-weight: 300;
  font-size: 15px;
  font-weight: 400;
`;
