import { useContext, useState } from "react";
import { addDoc, collection, doc, serverTimestamp } from "firebase/firestore";
import styled from "styled-components";
import { Button } from "@mui/material";
import { ThemeContext } from "../../contexts/ThemeContext";
import { db } from "../../firebase";

function ChatInput({ chatRef, roomId, roomName }) {
  const { theme } = useContext(ThemeContext);
  const [message, setMessage] = useState("");

  const sendMessage = async (e) => {
    e.preventDefault();
    if (!roomId || !message.trim()) return false;

    const docRef = doc(db, "channels", roomId);
    const colRef = collection(docRef, "messages");
    await addDoc(colRef, {
      message: message.trim(),
      timestamp: serverTimestamp(),
      user: "Denilson",
      avatar:
        "https://cdn.pixabay.com/photo/2016/08/08/09/17/avatar-1577909_1280.png",
    });

    chatRef?.current?.scrollIntoView({
      behavior: "smooth",
    });

    setMessage("");
  };

  return (
    <ChatInputContainer theme={theme}>
      <form>
        <input
          value={message}
          placeholder={`Message ${roomName}`}
          onChange={(e) => setMessage(e.target.value)}
        />
        <Button type="submit" onClick={sendMessage}>
          Send
        </Button>
      </form>
    </ChatInputContainer>
  );
}

export default ChatInput;

const ChatInputContainer = styled.div`
  border-radius: 20px;

  > form {
    position: relative;
    display: flex;
    justify-content: center;
  }

  > form > input {
    position: fixed;
    bottom: 20px;
    width: 70%;
    border: none;
    border-radius: 5px;
    padding: 15px;
    outline: none;
    background-color: ${(props) =>
      props.theme === "dark"
        ? "var(--secondary-color-dark)"
        : "var(--secondary-color-light)"};
    color: ${(props) =>
      props.theme === "dark"
        ? "var(--text-color-dark)"
        : "var(--text-color-light)"};
  }

  > form > button {
    display: none;
  }
`;
