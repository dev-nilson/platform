import { useContext, useEffect, useRef } from "react";
import { useSelector } from "react-redux";
import {
  collection,
  doc,
  getFirestore,
  orderBy,
  query,
} from "firebase/firestore";
import styled from "styled-components";
import { useCollection, useDocument } from "react-firebase-hooks/firestore";
import { ThemeContext } from "../../contexts/ThemeContext";
import { selectRoomId } from "../../redux/reducers/appSlice";
import InfoOutlinedIcon from "@mui/icons-material/InfoOutlined";
import ChatInput from "../ChatInput/ChatInput";
import Message from "../Message/Message";
import { app } from "../../firebase";
import { LineWobble } from "@uiball/loaders";

function Chat() {
  const { theme } = useContext(ThemeContext);
  const chatRef = useRef(null);
  const roomId = useSelector(selectRoomId);
  const [details] = useDocument(
    roomId && doc(getFirestore(app), "channels", roomId),
    {
      snapshotListenOptions: { includeMetadataChanges: true },
    }
  );
  const [messages, loading] = useCollection(
    roomId &&
      query(
        collection(getFirestore(app), "channels", roomId, "messages"),
        orderBy("timestamp", "asc")
      )
  );

  useEffect(() => {
    chatRef?.current?.scrollIntoView({
      behavior: "smooth",
    });
  }, [roomId, loading]);

  if (!roomId) return null;

  return (
    <ChatContainer theme={theme}>
      {details && messages ? (
        <>
          <Header theme={theme}>
            <HeaderTop>
              <h3>#{details?.data().name}</h3>
            </HeaderTop>
            <HeaderBottom>
              <InfoOutlinedIcon />
              <p>Channel description</p>
            </HeaderBottom>
          </Header>

          {messages?.docs.length > 0 ? (
            <ChatMessages>
              {messages?.docs.map((doc) => {
                const { message, timestamp, user, avatar } = doc.data();
                return (
                  <Message
                    key={doc.id}
                    message={message}
                    timestamp={timestamp}
                    user={user}
                    avatar={avatar}
                  />
                );
              })}
              <ChatBottom ref={chatRef} />
            </ChatMessages>
          ) : (
            <ChatWarning>
              <span>😞</span>
              <h2>No messages yet</h2>
            </ChatWarning>
          )}
          <ChatInput
            chatRef={chatRef}
            roomId={roomId}
            roomName={`#${details?.data().name}`}
          />
        </>
      ) : (
        <LoaderContainer>
          <LineWobble
            size={200}
            color={theme === "dark" ? "#f2f3f4" : "#1a1c1e"}
          />
        </LoaderContainer>
      )}
    </ChatContainer>
  );
}

export default Chat;

const Header = styled.div`
  justify-content: space-between;
  padding: 15px;
  border-bottom: ${(props) =>
    props.theme === "dark"
      ? "1px solid var(--secondary-color-dark)"
      : "1px solid var(--secondary-color-light)"};
  background-color: ${(props) =>
    props.theme === "dark"
      ? "var(--secondary-color-dark)"
      : "var(--secondary-color-light)"};
`;

const HeaderTop = styled.div``;

const HeaderBottom = styled.div`
  display: flex;
  align-items: center;

  > .MuiSvgIcon-root {
    margin-right: 5px;
    font-size: 18px;
  }

  > p {
    font-size: 15px;
  }
`;

const ChatContainer = styled.div`
  flex: 0.7;
  flex-grow: 1;
  margin-top: 60px;
  overflow-y: scroll;
  color: ${(props) =>
    props.theme === "dark"
      ? "var(--text-color-dark)"
      : "var(--text-color-light)"};
`;

const ChatBottom = styled.div`
  padding-bottom: 100px;
`;

const ChatMessages = styled.div``;

const ChatWarning = styled.div`
  margin-top: 25px;
  display: flex;
  flex-direction: column;
  align-items: center;

  > span {
    opacity: 0.7;
    font-size: 50px;
    margin: 0;
  }

  > h2 {
    margin: -10px;
    color: lightgray;
  }
`;

const LoaderContainer = styled.div`
  display: grid;
  align-items: center;
  justify-content: center;
  height: 50%;
`;
