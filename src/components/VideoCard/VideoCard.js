import { useContext } from "react";
import styled from "styled-components";
import { ThemeContext } from "../../contexts/ThemeContext";

function VideoCard({ title, description }) {
  const { theme } = useContext(ThemeContext);

  return (
    <VideoContainer>
      {/* <Video theme={theme} controls controlsList="nodownload">
        <source src={classVideo} type="video/mp4" />
      </Video> */}
      <VideoInfo theme={theme}>
        <VideoAvatar
          src="https://lippianfamilydentistry.net/wp-content/uploads/2015/11/user-default.png"
          alt=""
        />
        <div>
          <h3>{title}</h3>
          <p>{description}</p>
        </div>
      </VideoInfo>
    </VideoContainer>
  );
}

export default VideoCard;

const Video = styled.video`
  width: 300px;
  height: fit-content;
  border-radius: 10px;
`;

const VideoContainer = styled.div``;

const VideoInfo = styled.div`
  padding: 5px 10px;
  display: flex;
  gap: 15px;

  > div {
    display: flex;
    flex-direction: column;
    justify-content: center;

  }
  > div h3 {
    font-size: 16px;
    font-weight: 600;
  }

  > div p {
    font-size: 14px;
    font-weight: 200;
  }
`;

const VideoAvatar = styled.img`
  width: 40px;
  height: 40px;
  border-radius: 50%;
  object-fit: cover;
`;
