import { useContext } from "react";
import styled from "styled-components";
import { ThemeContext } from "../../contexts/ThemeContext";

function Activities() {
  const { theme } = useContext(ThemeContext);

  return <ActivitiesContainer theme={theme}>Activities</ActivitiesContainer>;
}

export default Activities;

const ActivitiesContainer = styled.div`
  flex: 0.7;
  flex-grow: 1;
  margin-top: 60px;
  overflow-y: scroll;
  color: ${(props) =>
    props.theme === "dark"
      ? "var(--text-color-dark)"
      : "var(--text-color-light)"};
`;
