import { useContext } from "react";
import styled from "styled-components";
import { ThemeContext } from "../../contexts/ThemeContext";
import { formatTimestamp } from "../../utils";

function Message({ message, timestamp, user, avatar }) {
  const { theme } = useContext(ThemeContext);

  return (
    <MessageContainer theme={theme}>
      <img src={avatar} alt="user profile" />
      <MessageInfo>
        <h4>
          {user}
          <span>
            {formatTimestamp(
              timestamp?.seconds * 1000 + timestamp?.nanoseconds / 1000000
            )}
          </span>
        </h4>
        <p>{message}</p>
      </MessageInfo>
    </MessageContainer>
  );
}

export default Message;

const MessageContainer = styled.div`
  display: flex;
  align-items: center;
  padding: 12px 24px;

  :hover {
    background-color: ${(props) =>
      props.theme === "dark"
        ? "var(--secondary-color-dark)"
        : "var(--secondary-color-light)"};
  }

  > img {
    height: 50px;
    border-radius: 7px;
  }
`;

const MessageInfo = styled.div`
  padding-left: 15px;

  > h4 > span {
    color: gray;
    font-weight: 300;
    font-size: 12px;
    margin-left: 10px;
  }
`;
