import { useContext } from "react";
import { ThemeContext } from "../../contexts/ThemeContext";
import styled from "styled-components";
import VideoCard from "../VideoCard/VideoCard";

function Classes() {
  const { theme } = useContext(ThemeContext);

  return (
    <ClassesContainer theme={theme}>
      <VideosGrid>
        <VideoCard title="Verb To Be" description="Semana 1" />
        <VideoCard title="Verb To Be" description="Semana 1" />
        <VideoCard title="Verb To Be" description="Semana 1" />
        <VideoCard title="Verb To Be" description="Semana 1" />
        <VideoCard title="Verb To Be" description="Semana 1" />
      </VideosGrid>
    </ClassesContainer>
  );
}

export default Classes;

const VideosGrid = styled.div`
  display: flex;
  justify-content: center;
  flex-wrap: wrap;
  row-gap: 50px;
  column-gap: 25px;
`

const ClassesContainer = styled.div`
  flex: 0.7;
  flex-grow: 1;
  padding: 20px;
  margin-top: 60px;
  overflow-y: scroll;
  color: ${(props) =>
    props.theme === "dark"
      ? "var(--text-color-dark)"
      : "var(--text-color-light)"};
`;
