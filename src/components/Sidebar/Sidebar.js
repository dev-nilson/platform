import { useContext, useState } from "react";
import { collection, getFirestore } from "firebase/firestore";
import { useCollection } from "react-firebase-hooks/firestore";
import styled from "styled-components";
import SidebarOption from "../SidebarOption/SidebarOption";
import { Avatar } from "@mui/material";
import {
  VideoLibrary as VideoLibraryIcon,
  Extension as ExtensionIcon,
  RocketLaunch as RocketLaunchIcon,
  InsertDriveFile as InsertDriveFileIcon,
  ExpandMore as ExpandMoreIcon,
  ExpandLess as ExpandLessIcon,
  Add as AddIcon,
} from "@mui/icons-material";
import { ThemeContext } from "../../contexts/ThemeContext";
import { app } from "../../firebase";

function Sidebar({ setCurrentPage }) {
  const [showWeeks, setShowWeeks] = useState(true);
  const { theme } = useContext(ThemeContext);
  const [channels] = useCollection(collection(getFirestore(app), "channels"));

  return (
    <SidebarContainer theme={theme}>
      <SidebarHeader theme={theme}>
        <SidebarInfo>
          <h3>
            <AvatarWrapper>
              <Avatar>D</Avatar>
              <AvatarStatus />
            </AvatarWrapper>
            &nbsp;Denilson Lemus
          </h3>
        </SidebarInfo>
      </SidebarHeader>
      <div onClick={() => setCurrentPage("classes")}>
        <SidebarOption Icon={VideoLibraryIcon} label="Classes" />
      </div>
      <div onClick={() => setCurrentPage("activities")}>
        <SidebarOption Icon={ExtensionIcon} label="Activities" />
      </div>
      <div onClick={() => setCurrentPage("projects")}>
        <SidebarOption Icon={RocketLaunchIcon} label="Projects" />
      </div>
      <div onClick={() => setCurrentPage("tests")}>
        <SidebarOption Icon={InsertDriveFileIcon} label="Tests" />{" "}
      </div>

      <hr />
      {showWeeks ? (
        <div onClick={() => setShowWeeks(!showWeeks)}>
          <SidebarOption Icon={ExpandLessIcon} label="Hide Weeks" />
        </div>
      ) : (
        <div onClick={() => setShowWeeks(!showWeeks)}>
          <SidebarOption Icon={ExpandMoreIcon} label="Show Weeks" />
        </div>
      )}

      {showWeeks &&
        channels?.docs.map((doc) => (
          <div key={doc.id} onClick={() => setCurrentPage("chat")}>
            <SidebarOption label={doc.data().name} id={doc.id} />
          </div>
        ))}
      <SidebarOption Icon={AddIcon} label="Add Week" triggerAction={true} />
    </SidebarContainer>
  );
}

export default Sidebar;

const SidebarContainer = styled.div`
  color: ${(props) =>
    props.theme === "dark"
      ? "var(--text-color-dark)"
      : "var(--text-color-light)"};
  background-color: ${(props) =>
    props.theme === "dark"
      ? "var(--primary-color-dark)"
      : "var(--primary-color-light)"};
  flex: 0.3;
  border-top: ${(props) =>
    props.theme === "dark"
      ? "1px solid var(--secondary-color-dark)"
      : "1px solid var(--secondary-color-light)"};
  border-right: ${(props) =>
    props.theme === "dark"
      ? "1px solid var(--secondary-color-dark)"
      : "1px solid var(--secondary-color-light)"};
  max-width: 250px;
  margin-top: 60px;

  > hr {
    background-color: #313334;
    border-bottom: none;
  }
`;

const SidebarHeader = styled.div`
  display: flex;
  border-bottom: ${(props) =>
    props.theme === "dark"
      ? "1px solid var(--secondary-color-dark)"
      : "1px solid var(--secondary-color-light)"};
  padding: 12px;
`;

const SidebarInfo = styled.div`
  flex: 1;

  > h2 {
    font-size: 18px;
    font-weight: 800;
    margin-bottom: 5px;
  }

  > h3 {
    display: flex;
    font-size: 15px;
    font-weight: 500;
    gap: 10px;
    align-items: center;
  }

  > h3 > .MuiSvgIcon-root {
    margin-right: 10px;
  }
`;

const AvatarWrapper = styled.div`
  display: inline-block;
  position: relative;
`;

const AvatarStatus = styled.div`
  width: 13px;
  height: 13px;
  border-radius: 50%;
  border: 1px solid #fff;
  background-color: #1ED760;
  position: absolute;
  bottom: 0px;
  right: 0px;
`;
