import { useContext } from "react";
import styled from "styled-components";
import { ThemeContext } from "../../contexts/ThemeContext";
import DarkModeIcon from "@mui/icons-material/DarkMode";
import LightModeIcon from "@mui/icons-material/LightMode";
//import SearchIcon from "@mui/icons-material/Search";

function Header() {
  const { theme, toggleTheme } = useContext(ThemeContext);

  return (
    <HeaderContainer theme={theme}>
      <HeaderLeft>
        <h1>SpeakIT</h1>
      </HeaderLeft>

      {/* <HeaderSearch theme={theme}>
        <SearchIcon />
        <input placeholder="Search" />
      </HeaderSearch> */}

      <HeaderRight onClick={toggleTheme}>
        {theme === "light" ? <DarkModeIcon /> : <LightModeIcon />}
      </HeaderRight>
    </HeaderContainer>
  );
}

export default Header;

const HeaderContainer = styled.div`
  display: flex;
  position: fixed;
  width: 100%;
  align-items: center;
  justify-content: space-between;
  border-bottom: ${(props) =>
    props.theme === "dark"
      ? "1px solid var(--secondary-color-dark)"
      : "1px solid var(--secondary-color-light)"};
  padding: 10px 0;
  background-color: ${(props) =>
    props.theme === "dark"
      ? "var(--primary-color-dark)"
      : "var(--primary-color-light)"};
  color: ${(props) =>
    props.theme === "dark"
      ? "var(--text-color-dark)"
      : "var(--text-color-light)"};
`;

const HeaderLeft = styled.div`
  flex: 0.3;
  display: flex;
  align-items: center;
  margin-left: 20px;
  height: 40px;

  > h1 {
    font-weight: 600;
    cursor: pointer;
  }

  > h1:hover {
    opacity: 0.8;
  }
`;

/* const HeaderSearch = styled.div`
  flex: 0.4;
  opacity: 1;
  border-radius: 6px;
  background-color: ${(props) =>
    props.theme === "dark"
      ? "var(--secondary-color-dark)"
      : "var(--secondary-color-light)"};
  height: 30px;
  align-items: center;
  text-align: center;
  display: flex;
  padding: 0 50px;

  > input {
    background-color: transparent;
    border: none;
    flex: 1;
    text-align: center;
    outline: 0;
    color: ${(props) =>
      props.theme === "dark"
        ? "var(--text-color-dark)"
        : "var(--text-color-light)"};
  }
`; */

const HeaderRight = styled.div`
  flex: 0.3;
  display: flex;
  align-items: flex-end;

  > .MuiSvgIcon-root {
    margin-left: auto;
    margin-right: 20px;
    cursor: pointer;
  }
`;
