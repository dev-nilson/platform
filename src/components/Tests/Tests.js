import { useContext } from "react";
import styled from "styled-components";
import { ThemeContext } from "../../contexts/ThemeContext";

function Tests() {
  const { theme } = useContext(ThemeContext);

  return <TestsContainer theme={theme}>Tests</TestsContainer>;
}

export default Tests;

const TestsContainer = styled.div`
  flex: 0.7;
  flex-grow: 1;
  margin-top: 60px;
  overflow-y: scroll;
  color: ${(props) =>
    props.theme === "dark"
      ? "var(--text-color-dark)"
      : "var(--text-color-light)"};
`;
