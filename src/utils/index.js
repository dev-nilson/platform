import moment from "moment";

export const formatTimestamp = (timestamp) => {
  const diffInMs = moment().diff(timestamp);
  const duration = moment.duration(diffInMs);

  if (duration.asSeconds() < 60) {
    return "just now";
  } else if (duration.asHours() < 1) {
    return `${Math.floor(duration.asMinutes())} minute${
      duration.asMinutes() > 1 ? "s" : ""
    } ago`;
  } else if (duration.asDays() < 1) {
    return `${Math.floor(duration.asHours())} hour${
      duration.asHours() > 1 ? "s" : ""
    } ago`;
  } else if (duration.asDays() < 7) {
    return `${Math.floor(duration.asDays())} day${
      duration.asDays() > 1 ? "s" : ""
    } ago`;
  } else {
    return moment(timestamp).format("MMM DD, YYYY");
  }
};
