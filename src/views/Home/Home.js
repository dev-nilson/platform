import { useState } from "react";
import styled from "styled-components";
import Header from "../../components/Header/Header";
import Sidebar from "../../components/Sidebar/Sidebar";
import Chat from "../../components/Chat/Chat";
import Classes from "../../components/Classes/Classes";
import Activities from "../../components/Activities/Activities";
import Projects from "../../components/Projects/Projects";
import Tests from "../../components/Tests/Tests";
import { ThemeContext } from "../../contexts/ThemeContext";
import "./Home.css";

function Home() {
  const [theme, setTheme] = useState("dark");
  const [currentPage, setCurrentPage] = useState("classes");

  const toggleTheme = () => {
    setTheme((state) => (state === "light" ? "dark" : "light"));
  };

  return (
    <ThemeContext.Provider value={{ theme, toggleTheme }}>
      <div id={theme}>
        <Header />
        <HomeBody>
          <Sidebar setCurrentPage={setCurrentPage} />
          {currentPage === "chat" && <Chat />}
          {currentPage === "classes" && <Classes />}
          {currentPage === "activities" && <Activities />}
          {currentPage === "projects" && <Projects />}
          {currentPage === "tests" && <Tests />}
        </HomeBody>
      </div>
    </ThemeContext.Provider>
  );
}

export default Home;

const HomeBody = styled.div`
  display: flex;
  height: 100vh;
`;
